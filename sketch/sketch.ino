//

const int L4 = 3;
const int L3 = 5;
const int L2 = 10;
const int L1 = 11;

const int x0y0 = 0;
const int x0y1 = 1;
const int x0y2 = 2;
const int x0y3 = 4;

const int x1y0 = 6;
const int x1y1 = 7;
const int x1y2 = 8;
const int x1y3 = 12;

const int x2y0 = 13;
const int x2y1 = 9;
const int x2y2 = A5;
const int x2y3 = A4;

const int x3y0 = A3;
const int x3y1 = A2;
const int x3y2 = A1;
const int x3y3 = A0;


const int allLeds[] = {x0y0,x0y1,x0y2,x0y3,x1y0,x1y1,x1y2,x1y3,x2y0,x2y1,x2y2,x2y3,x3y0,x3y1,x3y2,x3y3};
const int ledCount = 16;

const int allLayers[] = {L1,L2,L3,L4};
const int layerCount = 4;

void setup() {
  for(int i = 0; i<ledCount; i++){
    pinMode(allLeds[i], OUTPUT);
  }
  for(int i = 0; i<layerCount; i++){
    pinMode(allLayers[i], OUTPUT);
  }
  turnOffAll();
}

void loop() {
  lightUpEachColumn();

  slidingLayers(5, 250);

  lightUpAllLeds(4000, 100);
  fadeInAndOutAllLeds(5);
  fadeInAndOutEachLed();

  epilepsy(5);
}

void fadeInAndOutAllLeds(int count){
  turnOffAll();

  for(int j = 0; j<ledCount; j++){
    digitalWrite(allLeds[j], LOW);
  }
  for(int i = 0; i<count;i++){
    for(int j = 0; j<255;j++){
      for(int h = 0; h<layerCount; h++){
        analogWrite(allLayers[h], j);
        delay(1);
      }
    }
    for(int j = 255; j>=0;j--){
      for(int h = 0; h<layerCount; h++){
        analogWrite(allLayers[h], j);
        delay(1);
      }
    }
   }

  turnOffAll();
}

void fadeInAndOutEachLed() {
  for(int b = 0; b<layerCount; b++){
      for(int j = 0; j<ledCount; j++){
        digitalWrite(allLeds[j], LOW);

        for(int i = 0; i<256; i+=7){
          analogWrite(allLayers[b],i);
          delay(1);
        }
        for(int i = 255; i>=0; i-=7){
          analogWrite(allLayers[b],i);
          delay(1);
        }

        digitalWrite(allLeds[j], HIGH);
    }
  }
}

void lightUpAllLeds(int duration, int brightness){
  for(int i = 0; i<layerCount; i++){
   analogWrite(allLayers[i], brightness); 
  }
  for(int i = 0; i<ledCount; i++){
    digitalWrite(allLeds[i], LOW);
  }
  delay(duration);
  turnOffAll(); 
}

void lightUpEachColumn(){
  turnOffAll();
  turnOnAllLayers();

  for(int i = 0; i<ledCount;i++){
    digitalWrite(allLeds[i], LOW);
    delay(100);
  }

  turnOffAll();
}

void slidingLayers(int reps, int pause){ //WIP
  turnOffAll();

  turnOnAllLeds(); 

  for(int i = 0; i<reps; i++){
    bool directionUp = true;
    int offset = -3; // -3 to 3

    for(int j = 0; j<layerCount;j++){
      if(j+offset>layerCount || !(j+offset < 0)){
        digitalWrite(allLayers[j+offset],HIGH);
        delay(pause);
        digitalWrite(allLayers[j+offset],LOW);
      }

      offset+= directionUp ? 1 : -1;
      if((directionUp && offset == 3) || (!directionUp && offset == -3)) directionUp=!directionUp;
    }
  }

  turnOffAll();
}

void epilepsy(int count){
  turnOffAll();

  turnOnAllLeds(); 

  for(int i = 0; i<count;i++){
    turnOnAllLayers(); 
    delay(50);
    turnOffAllLayers();
    delay(50);
  }
}

void turnOffAllLayers(){
  for(int i = 0; i<layerCount; i++){
   digitalWrite(allLayers[i], LOW);
  }
}

void turnOffAllLeds(){
  for(int i = 0; i<ledCount; i++){
    digitalWrite(allLeds[i], HIGH);
  }
}

void turnOffAll(){
  turnOffAllLayers(); 
  turnOffAllLeds(); 
}

void turnOnAllLeds(){
 for(int i = 0; i<ledCount;i++){
    digitalWrite(allLeds[i], LOW);
  } 
}

void turnOnAllLayers(){
  for(int i = 0;i<layerCount; i++){
    digitalWrite(allLayers[i], HIGH);
  }
}