## How to use

1. Place the LED cube with the case on an Arduino **Uno** (there is a prototyping board inside the case that is designed to fit Arduino Uno only) - two pins at the end of each pin row are left out, both on the side closer to the USB port

2. Compile and flash the sketch onto the Arduino

3. Enjoy!
